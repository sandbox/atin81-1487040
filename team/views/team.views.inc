<?php
// $Id$ 

/**
*  This file is used to tell the views module about the new tournaments table.
*
 */
function team_views_data()  {
  $data = array();

  // ----------------------------------------------------------------
  // eSM_Equipo table
  //  
  $data['eSM_Equipo']['table']['group']  = t('Teams');  
  // Advertise this table as a possible base table
  $data['eSM_Equipo']['table']['base'] = array(
    'field' => 'nid_Equipo',
    'title' => t('Teams'),
    'help' => t("Teams based views."),
    'weight' => -10,
  );
  // When using the new 'Team' type you need to use relationships
  //   to access fields in other tables.
  // Relationship to the 'Node' table
  $data['eSM_Equipo']['nid_Equipo'] = array(
    'title' => t('Team'),
    'help' => t('The node describes the team.'),
    'relationship' => array(
      'label' => t('Team Node'),
      'base' => 'node',
      'base field' => 'nid',
      // This allows us to not show this relationship if the base is already
      // node_revisions so users won't create circular relationships.
      'skip base' => array('node'),
    ),
  );
  
  return $data;
}

/**
 * Implementation of hook handlers
 */
function team_views_handlers() {

}