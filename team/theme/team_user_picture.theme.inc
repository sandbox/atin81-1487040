<?php
/**
 * @file
 * User picture themeable functions
 */

/**
 * Display user picture
 */
function theme_team_user_picture($user, $preset = NULL, $link = FALSE) {
  // If user pictures are enabled...
  if (variable_get('user_pictures', 0)) {
    // Get the user's avatar if they have one or the default picture if exists.
    if (!empty($user->picture) && file_exists($user->picture)) {
      // We only want to get the full URL if not using imagecache.
      $picture = (!empty($preset) && module_exists('imagecache')) ? $user->picture : file_create_url($user->picture);
    }
    elseif (variable_get('user_picture_default', '')) {
      $picture = variable_get('user_picture_default', '');
    }
    // If there's a preset set and imagecache is enabled...
    if (!empty($preset) && module_exists('imagecache')) {
      $user_picture = theme('imagecache', $preset, $picture, $jugador->name, $jugador->name);
    }
    else {
      $user_picture = theme('image', $picture, $jugador->name, $jugador->name);
    }
    //Display image as link if $link=TRUE
    if ($link)
      $user_picture = l($user_picture, 'user/'. $user->uid);
  }
  return $user_picture;
}