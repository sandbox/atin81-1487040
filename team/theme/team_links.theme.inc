<?php
/**
 * @file
 * Team links theme-able functions
 */

/**
 * Approve to pending team invitation link
 */
function theme_team_user_invite_approve_link($tid, $uid) {
  return l(
    t('Approve'),
    "node/{$tid}/join/{$uid}",
    array(
      'title' => array('title' => t('Approve')),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}

/**
 * Disapprove to pending team invitation link
 */
function theme_team_user_invite_disapprove_link($tid, $uid) {
  return l(
    t('Disapprove'),
    "node/{$tid}/decline/{$uid}",
    array(
      'title' => array('title' => t('Disapprove')),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}

/**
 * Approve to pending team invitation link
 */
function theme_team_user_request_approve_link($tid, $uid) {
  return l(
    t('Approve'),
    "node/{$tid}/roster/request/{$uid}/approve",
    array(
      'title' => array('title' => t('Approve')),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}

/**
 * Disapprove to pending team invitation link
 */
function theme_team_user_request_disapprove_link($tid, $uid) {
  return l(
    t('Disapprove'),
    "node/{$tid}/roster/request/{$uid}/disapprove",
    array(
      'title' => array('title' => t('Disapprove')),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}

/**
 * Team's title link
 */
function theme_team_name($team) {
  return l(
    $team->title,
    "node/{$team->nid}",
    array (
      
    )
  );
}