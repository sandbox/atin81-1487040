<?php
// $Id$

/**
 * @file
 * Menu callbacks
 */

/**
 * List notifications
 *
 * @TODO 
 * - Help me group the results in the query
 * - Output RSS feed functionality
 */
function team_notifications_list_page() {
  global $user;

  $account = $user;
	$notifications = team_notifications_get_notifications($account);
	drupal_add_css(drupal_get_path('module', 'team_notifications') . '/team_notifications.css', 'module', 'all', TRUE);
	if ($notifications) {
		foreach ($notifications as $notification) {
				$row = array(
					theme('team_pane', $notification['team'], variable_get('team_notifications_picture_preset', '')),
					$notification['body'] . ' ('. $notification['links'] .')',
				);
				$rows[] = $row;
		}
		
		return '<div class="team-notifications-list-page">'. theme('table', array(), $rows, array('class' => 'team-notifications-table')) .'</div>';
	}
	else {
		return t('No Notifications found');
	}
}
