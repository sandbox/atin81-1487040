<?php
/**
 * @file
 * Template for team notificatios block
 * List all pending requests and provide links to the actions that can be taken on those requests
 */
drupal_add_css(drupal_get_path('module', 'team_notifications') . '/team_notifications.css', 'module', 'all', TRUE);
$count = 0;
if ($notifications) {
  foreach ($notifications as $notification) {
    if($count < $limit) {
      $row = array(
        theme('team_pane', $notification['team'], variable_get('team_notifications_picture_preset', '')),
        $notification['body'] . ' ('. $notification['links'] .')',
      );
      $rows[] = $row;
    }
    else {
      $more = TRUE;
      break;
    }
    $count++;
  }
  
  $output = theme('table', array(), $rows, array('class' => 'team-notifications-table'));
  if($more)
    $output .= l('More Notifications', 'team-notifications');
    
  print $output;
}
else {
  print t('No Notifications found');
}

?>
