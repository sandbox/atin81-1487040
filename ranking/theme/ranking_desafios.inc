<?php

/**
 * Función para hacer la personalización de la vista de puntuaciones de desafíos
 */
function theme_ranking_desafios ($form) {
    //die('<pre>' . print_r($form, TRUE) . '</pre>');
    
    $output = t('Type the amount of ranking points teams win/losse playing matches');
    $output .= t('Team with better ranking on left fields an team with worse ranking on right fields');
    
    $header = array(t('Points Diff'), t('(Better Team) Points Awarded by Win Match'), t('(Better Team) Points Substracted by Loss Match'), t('(Worse Team) Points Awarded by Win Match'), t('(Worse Team) Points Substracted by Loss Match')) ;
    $rows = array();
    
    for ($i = 0; $i < 11; $i++){
        $row = array();
        $row[] = drupal_render($form['Diferencia_'. $i]);
        $row[] = drupal_render($form['Equipo_Superior_Win_'. $i]);
        $row[] = drupal_render($form['Equipo_Superior_Loss_'. $i]);
        $row[] = drupal_render($form['Equipo_Inferior_Win_'. $i]);
        $row[] = drupal_render($form['Equipo_Inferior_Loss_'. $i]);
        $rows[] = $row;
    }
    
    $output .= theme('table', $header, $rows);
    
    
    return $output . drupal_render($form);
}