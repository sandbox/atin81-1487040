<?php
/**
 * Implements hook_default_rules_configuration().
 */
function torneo_default_rules_configuration() {
	$config = array (
		'rules' => 
		array (
			'rules_tournament_create_competition_schema' => 
			array (
				'#type' => 'rule',
				'#set' => 'event_signup_rules_event_close',
				'#label' => 'Tournament create competition schema',
				'#active' => 1,
				'#weight' => '0',
				'#categories' => 
				array (
					0 => 'tournament',
				),
				'#status' => 'custom',
				'#conditions' => 
				array (
				),
				'#actions' => 
				array (
					0 => 
					array (
						'#info' => 
						array (
							'label' => 'Start Event',
							'module' => 'Tournament',
							'arguments' => 
							array (
								'node' => 
								array (
									'type' => 'node',
									'label' => 'Tournament',
								),
							),
						),
						'#name' => 'torneo_action_start_event',
						'#settings' => 
						array (
							'#argument map' => 
							array (
								'node' => 'node',
							),
						),
						'#type' => 'action',
						'#weight' => 0,
					),
				),
				'#version' => 6003,
			),
			'rules_turnament_finish_event' => 
			array (
				'#type' => 'rule',
				'#set' => 'event_torneo_finished',
				'#label' => 'turnament finish event',
				'#active' => 1,
				'#weight' => '0',
				'#categories' => 
				array (
					0 => 'tournament',
				),
				'#status' => 'custom',
				'#conditions' => 
				array (
				),
				'#actions' => 
				array (
					0 => 
					array (
						'#weight' => 0,
						'#info' => 
						array (
							'label' => 'End Tournament',
							'module' => 'Tournament',
							'arguments' => 
							array (
								'node' => 
								array (
									'type' => 'node',
									'label' => 'Tournament',
								),
							),
						),
						'#name' => 'torneo_action_end_event',
						'#settings' => 
						array (
							'#argument map' => 
							array (
								'node' => 'content',
							),
						),
						'#type' => 'action',
					),
				),
				'#version' => 6003,
			),
		),
	);
  return $config;
}