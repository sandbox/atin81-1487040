<?php

/**
 * Theme team status for every tournament.
 */
function theme_tournament_team_signup_status ($status) {
		if($status == SIGNUP) {
				$output = '<span class="ready">
								'. t('Team Ready') .'
								</span>';
		}
		else {
				$output = '<span class="no-ready">
								'. t('Team Not Ready') .'
								</span>';
		}
    return $output;
}