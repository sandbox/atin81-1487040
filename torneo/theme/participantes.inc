<?php

/**
 * Theme tournament teams participants list.
 */
function theme_participantes ($form) {
    foreach (element_children($form['participants']) as $element) {
      $row = array();
      $row[] = drupal_render($form['participants'][$element]['name']);
      $row[] = drupal_render($form['participants'][$element]['players']);
      $rows[] = $row;
    }
    
    $output .= theme('table', $header, $rows, array('id' => 'tournament_participants'));
    $output .= theme('pager', NULL, $form['total']['#value'], 0);
    return $output;
}