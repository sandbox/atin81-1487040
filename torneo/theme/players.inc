<?php

/**
 * Theme tournament teams participants list.
 */
function theme_tournament_players ($players) {
   $output = theme('array_table', $players, 3, array('id' => 'players'));
    //Grap the output on divs needed for theme the list
    $output = '<div class="tournament-players-content">
                <div class="tournament-players-top">
                    <div class="tournament-players-top-left"></div>
                    <div class="tournament-players-top-right"></div>
                </div><!-- /.tournament-players-top --> 
                <div class="tournament-players-middle">
                    <div class="tournament-players-middle-left">
                        <div class="tournament-players-middle-right">
                            '. $output .'
                            <div class="clearfix"></div>
                        </div><!-- /.tournament-players-middle-right --> 
                    </div><!-- /.tournament-players-middle-left --> 
                </div><!-- /.tournament-players-middle --> 
                <div class="tournament-players-bottom">
                    <div class="tournament-players-bottom-left"></div>
                    <div class="tournament-players-bottom-right"></div>
                </div><!-- /.tournament-players-bottom -->  
                <div class="tournament-players-arrow"></div>
            </div>';
            
    return $output;
}