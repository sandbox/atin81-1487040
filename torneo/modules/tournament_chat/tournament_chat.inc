<?php
/**
 *	@file
 *	Form callbacks for Image for Team
 */
/**
 * Function to display admin form
 */
function tournament_chat_settings() {
	$options = node_get_types('names');
	$form['content'] = array(
		'#type' 					=> 'select',
		'#title' 					=> t('Select the conte type to attach chat nodes'),
		'#options' 				=> $options,
		'#default_value' 	=> variable_get('tournament_type', ''),
	);
	$form['submit'] = array(
		'#type' 					=> 'submit',
		'#value' 					=> t('Save'),
	);
	return $form;
}
/**
 * Implementation of hook submit for tournament_chat_admin
 */
function tournament_chat_settings_submit($form, $form_state) {
	variable_set('tournament_type', $form_state['values']['content']);
	drupal_set_message(t('Options saved'));
}