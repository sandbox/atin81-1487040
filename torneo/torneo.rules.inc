<?php
/**
 * @file
 *   Rules integration for the tournament module.
 */
/**
 * Implementation of hook_rules_event_info().
 */
function torneo_rules_event_info() {
  return array(
    'torneo_finished' => array(
      'label' => t('Update team stats when tournament finish.'),
      'module' => 'Tournament',
      'arguments' => array(
        'content' => array('type' => 'node', 'label' => t('Tournament that finished.')),
      ),
    ),
	);
}
/**
 * Implementation of hook_rules_action_info().
 */
function torneo_rules_action_info() {
  return array(
    'torneo_action_start_event' => array(
      'label' 		=> t('Start Event'),
      'module' 		=> 'Tournament',
			'help'			=> t('Create Competition Scheme for Tournaments'),
			'arguments' => array(
				'node' => array('type' => 'node', 'label' => t('Tournament')),
			),
    ),
    'torneo_action_end_event' => array(
      'label' 		=> t('End Tournament'),
      'module' 		=> 'Tournament',
			'help'			=> t('Update teams stats when finish competition'),
			'arguments' => array(
				'node' => array('type' => 'node', 'label' => t('Tournament')),
			),
    ),
  );
}
/**
 * Create competition scheme for tournaments
 */
function torneo_action_start_event($node) {
	_torneo_create_brackets($node->nid);
	_torneo_update_estado($node->nid, PRESENTE);
}
/**
 * Update teams stats when tournament finish
 */
function torneo_action_end_event($node) {
	_torneo_finaliza_torneo($node->nid);
}