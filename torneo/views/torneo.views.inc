<?php
// $Id$ 

/**
*  This file is used to tell the views module about the new tournaments table.
*
 */
function torneo_views_data()  {
  // Basic table information.

  // status filter
  $data['eSM_Reto']['Estado'] = array(
    'title'       => t('Status'),
    'help'        => t('Match Status.'),
    'field'       => array(
      'handler'         => 'views_handler_field_numeric',
      'click sortable'  => TRUE,
     ),
    'filter'      => array(
      'handler'      => 'match_handler_filter_status',
    ),
    'sort'        => array(
      'handler'      => 'views_handler_sort',
    ),
  );

  $data = array();

  return $data;
}
