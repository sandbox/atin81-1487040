<?php
/**
 * Funci�n para finalizar los torneos
 */
function _torneo_ending_event() {
  //Selecciona todos los torneos en estado activo para ver si se terminaron
  $result = db_query('SELECT * FROM {eSM_Torneos} WHERE Estado = %d', PRESENTE);
    
  while ($torneo = db_fetch_object($result)) {
	$numero_equipos = torneo_get_total_participantes($torneo->nid_Torneo);
    
  //Encuentra el n�mero de rondas necesarias
	//para realizar el torneo.
    $bracketSize = 2;
    $numrounds = 2;
    while($numero_equipos > $bracketSize) {
      $numrounds++;
      $bracketSize = $bracketSize * 2;
    }
    //Encuentra si se han llevado a cabo todas las rondas del torneo
    $ronda_actual = db_fetch_array(db_query('SELECT * FROM {eSM_Torneos_Brackets}
                                            WHERE nid_Torneo = %d ORDER BY Ronda DESC LIMIT 1',
                                            $torneo->nid_Torneo));
        
    if($ronda_actual['Ronda'] == $numrounds) {
      $torneo = node_load(array('nid' => $torneo->nid_Torneo));
      rules_invoke_event('tournament_finished', $torneo);
    }
  }
}