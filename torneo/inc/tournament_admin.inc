<?php
/**
 *	@file
 *	Form callbacks for Image for Team
 */
/**
 * Gathers information on if a tournament has an image configured and offered to configure it if not.
 * As with some other functions, this is shamelessly taken from the Ubercart code.
 */
function tournament_admin(){
  if(!module_exists('imagefield') || !module_exists('imagecache')) {
    $status = 'warning';
    $form['message'] = t('To configure tournament image support, <a href="!url">enable</a> the <a href="http://drupal.org/project/imagecache">Imagecache</a> module.', array('!url' => url('admin/build/modules')));
  } else {
		// Get all the imagecache presets on the site.
		$options = array('' => '');
		$presets = imagecache_presets();
		foreach ($presets AS $preset) {
			$options[$preset['presetname']] = $preset['presetname'];
		}

    if ($options) {
			$form['preset'] = array(
				'#type' 					=> 'select',
				'#title' 					=> t('Tournament list picture preset'),
				'#options' 				=> $options,
				'#description' 		=> t("Imagecache preset to use for the tournament's participant logos. Leave blank to not use this feature."),
				'#default_value' 	=> variable_get('tournament_logo_preset', ''),
			);
			$form['submit'] = array (
				'#type' 					=> 'submit',
				'#value' 					=> t( 'Save' ),
			);
    }
    else {
      $form['message'] = t('<a href="!url">Click here</a> to add Imagecache Presets for tournament image support:', array('!url' => url('admin/build/imagecache')));
    }
  }
	
  return $form;
}
/**
 * Función para guardar el preset de imagecache con que se van a renderizar las listas de equipos
 */
function tournament_admin_submit($form, $form_state) {
	variable_set('tournament_logo_preset', $form_state['values']['preset']);
	drupal_set_message(t('Change saved'));
}