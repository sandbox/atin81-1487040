<?php
/**
 * Funci�n para mostrar la vista de los participantes y equipos registrados en un torneo
 */
function torneo_participantes () {
  $torneo = get_settings_torneo(arg(1));                                 //Obtiene las configuraciones del torneo del nid obtenido en la barra de direcci�n
	drupal_add_css(drupal_get_path('module', 'torneo') . '/css/participants.css', 'module', 'all', TRUE);
  
  //Obtiene la lista de participantes en el torneo
  $sql = 'SELECT * FROM {eSM_Torneos_Participantes} WHERE nid_Torneo = '. $torneo->nid_Torneo;
  $result = pager_query($sql, 10);
	//Get total number of teams
	$total = db_result(db_query('SELECT COUNT(*) FROM {eSM_Torneos_Participantes}
															WHERE nid_Torneo = %d',
															$torneo->nid_Torneo));
  
  if ($total == 0) {
    $form['mensaje'] = array (
      '#value' => t('No team have signup for this tournament yet.'),
    );
  }
  else {
		//Display list of teams players with signup status
		while($team = db_fetch_object($result))  {
			$status = $team->status;
			$team = node_load(array('nid' => $team->nid_Equipo));
			$form['participants'][$team->nid]['name'] = array (
				'#type' => 'item',
				'#value' => '<div class="team-status">'.
											theme('team_pane', $team, variable_get('tournament_logo_preset', '')) .
											'<span>'. t('Team: ') .'</span>'.
											theme('team_name', $team) .
											theme('tournament_team_signup_status', $status) .
										'</div>',
			);
			//Load team players who signup on this tournament
			$players = array();
			$roster = _team_get_plantilla($team->nid);
			while ($player = db_fetch_object($roster)) {
				$player = user_load(array('uid' => $player->uid));
				
				if(_torneo_signup_esta_inscrito($player->uid, $torneo->nid_Torneo)) {
					$status = '<span class="signup" style="display:block;">'. t('Signup') .'</span>';
				}
				else {
					$status = '<span class="no-signup" style="display:block;">'. t('No Signup') .'</span>';
				}
				$players[] = theme('team_user_picture', $player, 'team_lists_picture_preset') .
											'<div class="team-info">'
												. l($player->name, 'user/'. $player->uid) .$status .
											'</div>';
			}
			
			$form['participants'][$team->nid]['players'] = array (
				'#type'   => 'item',
				'#value'  => theme('tournament_players', $players),
			);
		}
    $form['#theme'] = 'participantes';
		$form['total']['#value'] = $total;
  }
  
  return $form;
}