<?php
/**
 * Implements hook_default_rules_configuration().
 */
function torneo_signup_default_rules_configuration() {
	$config = array (
		'rules' => 
		array (
			'rules_team_signup' => 
			array (
				'#type' => 'rule',
				'#set' => 'event_signup_rules_event_insert',
				'#label' => 'user signup',
				'#active' => 1,
				'#weight' => '0',
				'#categories' => 
				array (
					0 => 'team_signup',
				),
				'#status' => 'custom',
				'#conditions' => 
				array (
				),
				'#actions' => 
				array (
					0 => 
					array (
						'#info' => 
						array (
							'label' => 'Team Signup',
							'arguments' => 
							array (
								'node' => 
								array (
									'type' => 'node',
									'label' => 'Tournament',
								),
								'user' => 
								array (
									'type' => 'user',
									'label' => 'User, who signup',
								),
							),
							'module' => 'Tournament Signup',
						),
						'#name' => 'torneo_signup_action_team',
						'#settings' => 
						array (
							'#argument map' => 
							array (
								'node' => 'node',
								'user' => 'user',
							),
						),
						'#type' => 'action',
						'#weight' => 0,
					),
				),
				'#version' => 6003,
			),
			'rules_user_cancel_signup' => 
			array (
				'#type' => 'rule',
				'#set' => 'event_signup_rules_event_cancel',
				'#label' => 'user cancel signup',
				'#active' => 1,
				'#weight' => '0',
				'#categories' => 
				array (
					0 => 'team_signup',
				),
				'#status' => 'custom',
				'#conditions' => 
				array (
				),
				'#actions' => 
				array (
					0 => 
					array (
						'#weight' => 0,
						'#info' => 
						array (
							'label' => 'Team Cancel Signup',
							'module' => 'Tournament Signup',
							'arguments' => 
							array (
								'user' => 
								array (
									'type' => 'user',
									'label' => 'Usuario',
								),
								'node' => 
								array (
									'type' => 'node',
									'label' => 'Contenido',
								),
							),
						),
						'#name' => 'torneo_signup_action_team_cancel_signup',
						'#settings' => 
						array (
							'#argument map' => 
							array (
								'user' => 'user',
								'node' => 'node',
							),
						),
						'#type' => 'action',
					),
				),
				'#version' => 6003,
			),
		),
	);
  return $config;
}