<?php
/**
 * @file
 *   Rules integration for the tournament_signup module.
 */
/**
 * Implementation of hook_rules_action_info().
 */
function torneo_signup_rules_action_info() {
  return array(
    'torneo_signup_action_team_signup' => array(
      'label' 		=> t('Team Signup'),
      'module' 		=> 'Tournament Signup',
			'help'			=> t('Update team signup status on user signup to content'),
			'arguments' => array(
				'user' => array('type' => 'user', 'label' => t('User')),
				'node' => array('type' => 'node', 'label' => t('Content')),
			),
    ),
    'torneo_signup_action_team_cancel_signup' => array(
      'label' 		=> t('Team Cancel Signup'),
      'module' 		=> 'Tournament Signup',
			'help'			=> t('Update team signup status on user cancel signup'),
			'arguments' => array(
				'user' => array('type' => 'user', 'label' => t('User')),
				'node' => array('type' => 'node', 'label' => t('Content')),
			),
    ),
  );
}
/**
 * Updates team signup status
 */
function torneo_signup_action_team_signup($user, $node) {
  foreach($node->taxonomy as $index => $term) {
		if($modalidad = term_is_active($term->tid)) {
			$team = _team_get_user_team_modalidad($user->uid, $modalidad->id_Modalidad_Juego_Temporada);
			//Add team record for this tournament
			if (!(_torneo_signup_check_team_signup_exists($team->nid, $node->nid)))
				_torneo_signup_team($team->nid, $node->nid);
			//Update the team signup status according to register players
			$players = _torneo_signup_players($team->nid, $node->nid);
			if ($players >= $modalidad->Minimo_Jugadores)
				_torneo_signup_confirm_team($team->nid, $node->nid);
		}
  }
}
/**
 * Updates the team signup status on user cancel signup
 */
function torneo_signup_action_team_cancel_signup($user, $node) {
  foreach($node->taxonomy as $index => $term) {
		if($modalidad = term_is_active($term->tid)) {
			$team = _team_get_user_team_modalidad($user->uid, $modalidad->id_Modalidad_Juego_Temporada);
			//Rule is executing before actually remove the user from event on database
			//a small hack (-1) needed to retrieve the actual number of players
			$players = _torneo_signup_players($team->nid, $node->nid) - 1;
			if ($players < $modalidad->Minimo_Jugadores)
				_torneo_signup_remove_team($team->nid, $node->nid);
		}
  }
}