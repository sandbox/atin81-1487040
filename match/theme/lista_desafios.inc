<?php

/**
 * Función para hacer la personalización de la vista de puntuaciones de desafíos
 */
function theme_match_lista_desafios ($form) {
    $output = '';
    
    $header = array(t('Oponent'), t('Date'), t('Result'), t('Points'), t('Match'));
    $rows = array();
    
    foreach (element_children($form) as $element)   {
        if ($element != "form_build_id" && $element != "form_token" && $element != "form_id") {
            $row = array();
            $row[] = drupal_render($form[$element]['Rival']);
            $row[] = drupal_render($form[$element]['Fecha']);
            $row[] = drupal_render($form[$element]['Resultado']);
            $row[] = drupal_render($form[$element]['Puntos']);
            $row[] = drupal_render($form[$element]['Partido']);
            $rows[] = $row;
        }
    }
    
    $output .= theme('table', $header, $rows);
    
    
    return $output . drupal_render($form);
}