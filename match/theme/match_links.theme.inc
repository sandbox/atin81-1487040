<?php
// $Id: user_relationships_ui.theme.inc,v 1.1.2.11 2010/02/23 22:45:38 alexk Exp $

/**
 * @file
 * User Relationships theme-able functions
 */

/**
 * Match send proofs link
 */
function theme_match_report_proofs_link($nid) {
  return l(
    t('Report'),
    "node/{$nid}/proof",
    array(
      'title' => array('title' => t('Report')),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}

/**
 * Match request approve link
 */
function theme_match_pending_request_approve_link($nid) {
  return l(
    t('Approve'),
    "node/{$nid}/accept",
    array(
      'title' => array('title' => t('Approve')),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}

/**
 * Match request disapprove link
 */
function theme_match_pending_request_disapprove_link($nid) {
  return l(
    t('Disapprove'),
    "node/{$nid}/cancel",
    array(
      'title' => array('title' => t('Disapprove')),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}

/**
 * Match's title with rival name link
 */
function theme_match_rival_name($name, $nid) {
  return l(
    $name,
    "node/{$nid}",
    array(
      'title' => array('title' => t('Match againts ', $name)),
      'query' => drupal_get_destination(),
      //'attributes' => array('class' => 'user_relationships_popup_link'),
    )
  );
}