<?php

/**
 * Theme an array into a table
 */
function theme_array_table ($array, $columns = 1, $attributes = array()) {
    $rows = array();
    $row = array();
    $count = 1;
    
    foreach($array as $index=>$element)   {
				if ($count <= $columns) {
            ++$count;
            $row[] = $element;
        }
        else {
            $rows[] = $row;
            $count = 2;
            $row = array();
            $row[] = $element;
        }
    }
    $rows[] = $row;
    
    return theme('table', $header, $rows, $attributes);
}