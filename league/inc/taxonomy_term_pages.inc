<?php

/**
 * Función para desplegar la vista de actividad de la comunidad en una página
 */
function taxonomy_term_comunity () {
  $tid = arg(2);
  $modalidad = term_is_active($tid);
  //Colocamos el título de la pestaña
  drupal_set_title(t('Comunidad de '. $modalidad->Nombre));
  
  //Agregamos el bloque para escribir nuevos mensaje
	if(module_exists('facebook_status')) {
		$block =  module_invoke('facebook_status', 'block' , 'view', 'facebook_status');
		$content = $block['content'];
	}
  
  //Agregamos la vista para mostrar la actividad
  $content .= views_embed_view('activity_stream_block', 'default');
  return $content;
}